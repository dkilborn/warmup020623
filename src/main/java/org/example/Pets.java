package org.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Pets {
    private final static HashMap<String, ArrayList<String>> classPets = new HashMap<>();

    public static void addToCollection(HashMap<String,ArrayList<String>> input){
        for (Map.Entry<String, ArrayList<String>> entry : input.entrySet()) {
            if (!classPets.containsKey(entry.getKey())){
                classPets.put(entry.getKey(),entry.getValue());
                continue;
            }
            for (String s : entry.getValue()) {
                classPets.get(entry.getKey()).add(s);
            }
        }
    }
    public static String formatPets(String name){
        if(!classPets.containsKey(name)){
            return name + " has no pets.";
        }
        if(classPets.get(name).size() == 1) {
            return name + " has one pet: " + classPets.get(name).toString().substring(1,classPets.get(name).toString().length()-1);
        }
        return multiPetFormatter(name);
    }
    private static String multiPetFormatter(String name) {
        StringBuilder myString = new StringBuilder();
        if(classPets.get(name).size()==2){
            myString.append(name).append(" has two pets: ");
            myString.append(classPets.get(name).get(0)).append(" and ").append(classPets.get(name).get(1));
            return myString.toString();
        }
        myString.append(name).append(" has ").append(EnglishNumberToWords.convert(classPets.get(name).size())).append(" pets: ");
        for (int i = 0; i < classPets.get(name).size(); i++) {
            if(i==0){
                myString.append(classPets.get(name).get(0));
                continue;
            }
            if(i == classPets.get(name).size()-1){
                myString.append(", and ").append(classPets.get(name).get(i));
            }
            else{
                myString.append(", ").append(classPets.get(name).get(i));
            }

        }
        return myString.toString();
    }
}
