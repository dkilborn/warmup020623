package org.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        HashMap<String, ArrayList<String>> test = new HashMap<>();
        test.put("Peter", new ArrayList<String>(Arrays.asList("Kiki","PJ")));
        test.put("Jean-Marc", new ArrayList<String>(Arrays.asList("Fido","Spot","Fluffy")));
        test.put("Dylan", new ArrayList<String>(List.of("Cinnamon")));
        Pets.addToCollection(test);
        System.out.println(Pets.formatPets("Peter"));
        System.out.println(Pets.formatPets("Dylan"));
        System.out.println(Pets.formatPets("Jean-Marc"));

    }
}